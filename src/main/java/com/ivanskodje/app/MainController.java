package com.ivanskodje.app;

import com.ivanskodje.app.database.DataHandler;
import com.ivanskodje.app.database.DatabaseConfig;
import com.ivanskodje.app.helpers.FileManagers.StringFileManager;
import com.ivanskodje.tvmazeapp.TVMazeApi;
import com.ivanskodje.tvmazeapp.model.Show;
import com.ivanskodje.tvmazeapp.model.ShowSchedule;
import com.ivanskodje.tvmazeapp.report.NextWeekReporter;
import com.ivanskodje.tvmazeapp.report.Reporter;
import com.ivanskodje.tvmazeapp.report.SummaryReporter;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;

public class MainController implements Initializable
{

	// FXML Root window
	@FXML
	private AnchorPane window;
	@FXML
	private TextArea textArea;
	@FXML
	private Label label;

	// Stored reports
	private Map<REPORT_TYPE, String> reports;

	// Paths
	private static final String NEXT_WEEK_PATH = "report_next_week.txt";
	private static final String SUMMARY_PATH = "report_summary.txt";
	private static final String BEST_EPISODE_PER_SHOW_PATH = "report_best_episode_per_show.txt";

	// Enum
	private static enum REPORT_TYPE
	{
		NONE,
		NEXT_WEEK,
		SUMMARY,
		BEST_EPISODE_PER_SHOW
	}

	@Override
	public void initialize(URL url, ResourceBundle rb)
	{
		// Reports
		reports = new HashMap<>();
	}

	/**
	 * TODO: Make sure we close connections to database before closing.
	 *
	 * @param event
	 */
	@FXML
	private void onQuitPressed(ActionEvent event)
	{
		Platform.exit();
	}

	@FXML
	private void onExportSummaryPressed(ActionEvent event)
	{
		boolean getSql = false;

		// For testing purposes:
		try
		{
			LocalDate loadedTime = DatabaseConfig.getInstance().getDayLoaded();

			if (loadedTime != null)
			{
				LocalDate currentDate = LocalDate.now();

				loadedTime.plusDays(1); // ADd 1 day

				// If we have exceeded 1 day since last time we loaded remote, fetch remote.
				if (currentDate.isAfter(loadedTime))
				{
					// load remote
					getSql = false;
				}
				else
				{
					getSql = true;
				}
			}
			else
			{
				// Load remote
				getSql = false;
			}
		}
		catch (Exception ex)
		{
			getSql = false;
		}

		// If we have not loaded report before, create it
		if (!reports.containsKey(REPORT_TYPE.SUMMARY))
		{
			List<Show> shows = new ArrayList<>();

			// Get via Database?
			if (getSql)
			{
				// Get all shows we have locally
				shows = DataHandler.getInstance().getAllShows();

				DatabaseConfig.getInstance().setDayLoaded(LocalDate.now());
				DatabaseConfig.getInstance().save();
			}
			else
			{
				// Get 250 shows
				shows = TVMazeApi.getInstance().getShowByIndex(0, true);
			}

			// Create report and store it
			Reporter report = new SummaryReporter();
			reports.put(REPORT_TYPE.SUMMARY, report.format(shows));

			// Store report to .txt file
			StringFileManager.saveFile(SUMMARY_PATH, reports.get(REPORT_TYPE.SUMMARY));

			// Populate local database
			Thread threadInsertIntoDB = new Thread(() ->
			{
				// TODO:
				// DataHandler.getInstance().insertShows(shows);
			});
			threadInsertIntoDB.start();
		}

		// Set label
		label.setText("SUMMARY (ALL STORED SHOWS)");

		// Set textarea
		textArea.setText(reports.get(REPORT_TYPE.SUMMARY));
	}

	@FXML
	private void onExportNextWeekPressed(ActionEvent event)
	{
		// If we have not loaded report before, create it
		if (!reports.containsKey(REPORT_TYPE.NEXT_WEEK))
		{
			// Get a full list of show schedules (no duplicates)
			List<ShowSchedule> showSchedules = TVMazeApi.getInstance().getWeeklyShowSchedules();

			// Create report and store it
			Reporter report = new NextWeekReporter();
			reports.put(REPORT_TYPE.NEXT_WEEK, report.format(showSchedules));

			// Store report to .txt file
			StringFileManager.saveFile(NEXT_WEEK_PATH, reports.get(REPORT_TYPE.NEXT_WEEK));

			// Populate local database
			Thread threadInsertIntoDB = new Thread(() ->
			{
				DataHandler.getInstance().insertWeeklyShowSchedules(showSchedules);
			});
			threadInsertIntoDB.start();
		}

		// Set label
		label.setText("NEXT WEEK REPORT (MON-SUN)");

		// Set textarea
		textArea.setText(reports.get(REPORT_TYPE.NEXT_WEEK));
	}
}
