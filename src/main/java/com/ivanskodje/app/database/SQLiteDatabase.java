/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivanskodje.app.database;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.ivanskodje.app.database.tables.DaysEntry;
import com.ivanskodje.app.database.tables.EpisodeEntry;
import com.ivanskodje.app.database.tables.NetworkEntry;
import com.ivanskodje.app.database.tables.ShowEntry;
import com.ivanskodje.app.database.tables.TimeEntry;
import com.ivanskodje.tvmazeapp.TVMazeApi;
import com.ivanskodje.tvmazeapp.model.Episode;
import com.ivanskodje.tvmazeapp.model.Network;
import com.ivanskodje.tvmazeapp.model.Show;
import com.ivanskodje.tvmazeapp.model.ShowSchedule;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ivan Skodje
 */
public class SQLiteDatabase implements IDatabase
{

	// Gson
	private static final Gson GSON = new Gson();

	// SQLite Database URL
	private static String databaseURL;

	// --- TABLES QUERIES ---
	// Create Days Table Query
	private static final String TABLE_DAYS_CREATE
			= "CREATE TABLE " + DaysEntry.TABLE_NAME + "("
			+ DaysEntry.COLUMN_SHOW_ID + " INTEGER NOT NULL,\n"
			+ DaysEntry.COLUMN_DAY + " VARCHAR(200) NOT NULL,\n"
			+ "FOREIGN KEY(" + DaysEntry.COLUMN_SHOW_ID + ") REFERENCES "
			+ ShowEntry.TABLE_NAME + "(" + ShowEntry.COLUMN_ID + ") ON UPDATE CASCADE ON DELETE CASCADE"
			+ ")";

	// Create Time Table Query
	private static final String TABLE_TIME_CREATE
			= "CREATE TABLE " + TimeEntry.TABLE_NAME + "("
			+ TimeEntry.COLUMN_SHOW_ID + " INTEGER NOT NULL,\n"
			+ TimeEntry.COLUMN_TIME + " DATE NOT NULL,\n"
			+ "FOREIGN KEY(" + TimeEntry.COLUMN_SHOW_ID + ") REFERENCES "
			+ ShowEntry.TABLE_NAME + "(" + ShowEntry.COLUMN_ID + ") ON UPDATE CASCADE ON DELETE CASCADE"
			+ ")";

	// Create Network Table Query
	private static final String TABLE_NETWORK_CREATE
			= "CREATE TABLE " + NetworkEntry.TABLE_NAME + "("
			+ NetworkEntry.COLUMN_ID + " INTEGER NOT NULL,\n"
			+ NetworkEntry.COLUMN_NAME + " VARCHAR(200) NOT NULL,\n"
			+ NetworkEntry.COLUMN_COUNTRY + " TEXT,\n"
			+ "PRIMARY KEY(" + NetworkEntry.COLUMN_ID + ")"
			+ ")";

	// Drop Show Table Query
	private static final String TABLE_NETWORK_DROP = "DROP TABLE " + NetworkEntry.TABLE_NAME;

	// Create Show Table Query
	private static final String TABLE_SHOW_CREATE
			= "CREATE TABLE " + ShowEntry.TABLE_NAME + "("
			+ ShowEntry.COLUMN_ID + " INTEGER NOT NULL,\n"
			+ ShowEntry.COLUMN_NAME + " VARCHAR(200) NOT NULL,\n"
			+ ShowEntry.COLUMN_RATING + " REAL,\n"
			+ ShowEntry.COLUMN_STATUS + " VARCHAR(200),\n"
			+ ShowEntry.COLUMN_NETWORK_ID + " INTEGER,\n"
			+ ShowEntry.COLUMN_JSON_CONTENT + " TEXT,\n"
			+ ShowEntry.COLUMN_DATE_CREATED + " DATE default CURRENT_DATE,\n"
			+ "PRIMARY KEY(" + ShowEntry.COLUMN_ID + "), \n"
			+ "FOREIGN KEY(" + ShowEntry.COLUMN_NETWORK_ID + ") REFERENCES "
			+ NetworkEntry.TABLE_NAME + "(" + NetworkEntry.COLUMN_ID + ") ON UPDATE CASCADE ON DELETE SET NULL"
			+ ")";

	// Drop Show Table Query
	private static final String TABLE_SHOW_DROP = "DROP TABLE " + ShowEntry.TABLE_NAME;

	// Create Episode Table Query
	private static final String TABLE_EPISODE_CREATE
			= "CREATE TABLE " + EpisodeEntry.TABLE_NAME + "("
			+ EpisodeEntry.COLUMN_ID + " INTEGER NOT NULL,\n"
			+ EpisodeEntry.COLUMN_SHOW_ID + " INTEGER NOT NULL,\n"
			+ EpisodeEntry.COLUMN_NAME + " VARCHAR(200) NOT NULL,\n"
			+ EpisodeEntry.COLUMN_SEASON + " INTEGER,\n"
			+ EpisodeEntry.COLUMN_NUMBER + " INTEGER,\n"
			+ EpisodeEntry.COLUMN_AIRDATE + " DATE,\n"
			+ EpisodeEntry.COLUMN_AIRTIME + " DATE,\n"
			+ EpisodeEntry.COLUMN_JSON_CONTENT + " TEXT,\n"
			+ EpisodeEntry.COLUMN_DATE_CREATED + " DATE default CURRENT_DATE,\n"
			+ "PRIMARY KEY(" + EpisodeEntry.COLUMN_ID + "),\n"
			+ "FOREIGN KEY(" + EpisodeEntry.COLUMN_SHOW_ID + ") REFERENCES "
			+ ShowEntry.TABLE_NAME + "(" + ShowEntry.COLUMN_ID + ") ON UPDATE CASCADE ON DELETE CASCADE"
			+ ")";

	// Drop Episode Table Query
	private static final String TABLE_EPISODE_DROP = "DROP TABLE " + EpisodeEntry.TABLE_NAME;

	/**
	 * SQLite requires only a name
	 *
	 * @param databaseName
	 */
	public SQLiteDatabase(String databaseName)
	{
		// Put database url together
		databaseURL = "jdbc:sqlite:" + databaseName + ".db";

		// Setup tables
		setupTables();

		// Setup pragma
		setupPragma();
	}

	/**
	 * Setups the tables. If the tables already exist, it will not attempt to
	 * insert.
	 */
	private void setupTables()
	{
		// Setup tables
		createTable(NetworkEntry.TABLE_NAME, TABLE_NETWORK_CREATE);
		createTable(ShowEntry.TABLE_NAME, TABLE_SHOW_CREATE);
		createTable(EpisodeEntry.TABLE_NAME, TABLE_EPISODE_CREATE);
		createTable(DaysEntry.TABLE_NAME, TABLE_DAYS_CREATE);
		createTable(TimeEntry.TABLE_NAME, TABLE_TIME_CREATE);
	}

	/**
	 * Connect to database, return connection
	 *
	 * @return
	 */
	private Connection connect()
	{
		Connection connection = null;
		try
		{
			// Setup driver and establish connection with SQLite Driver
			Class.forName("org.sqlite.JDBC");
			connection = DriverManager.getConnection(databaseURL);
		}
		catch (ClassNotFoundException ex)
		{
			System.err.println("ClassNotFoundException (connect): " + ex.getMessage());
		}
		catch (SQLException ex)
		{
			SQLExceptionHandler.printSQLiteExceptionMessage(ex);
		}

		return connection;
	}

	/**
	 * foreign_keys:
	 *
	 * We need to setup PRAGMA foreign_keys to true in order to get a cascading
	 * effect with the foreign keys in SQLite. It is by default set to false.
	 *
	 * @return
	 */
	private void setupPragma()
	{
		ResultSet resultSet = null;
		Statement statement = null;
		Connection connection = null;

		try
		{
			// Connect and execute query
			connection = connect();
			statement = connection.createStatement();
			statement.execute("PRAGMA foreign_keys = true;");
		}
		catch (SQLException ex)
		{
			SQLExceptionHandler.printSQLiteExceptionMessage(ex);
		}
		finally
		{
			// Disconnect
			disconnect(resultSet, statement, connection);
		}
	}

	/**
	 * Disconnect from database
	 *
	 * @param resultSet
	 * @param statement
	 * @param connection
	 */
	private void disconnect(ResultSet resultSet, Statement statement, Connection connection)
	{
		/**
		 * Attempt to close connections at the end
		 */
		if (resultSet != null)
		{
			try
			{
				resultSet.close();
			}
			catch (SQLException ex)
			{
				SQLExceptionHandler.printSQLiteExceptionMessage(ex);
			}
		}
		if (statement != null)
		{
			try
			{
				statement.close();
			}
			catch (SQLException ex)
			{
				SQLExceptionHandler.printSQLiteExceptionMessage(ex);
			}
		}
		if (connection != null)
		{
			try
			{
				connection.close();
			}
			catch (SQLException ex)
			{
				SQLExceptionHandler.printSQLiteExceptionMessage(ex);
			}
		}
	}

	/**
	 * Creates a table given a table name and query to execute
	 *
	 * @param tableName
	 * @param query
	 * @return
	 */
	private boolean createTable(String tableName, String query)
	{
		ResultSet resultSet = null;
		Statement statement = null;
		Connection connection = null;

		try
		{
			// Connect
			connection = connect();

			// Generate statement
			statement = connection.createStatement();
			DatabaseMetaData databaseMetaData = connection.getMetaData();
			resultSet = databaseMetaData.getTables(null, null, tableName, null);

			// If table does NOT already exist, create the table
			if (!resultSet.next())
			{
				statement.execute(query);
				return true;
			}
		}
		catch (SQLException ex)
		{
			SQLExceptionHandler.printSQLiteExceptionMessage(ex);
		}
		finally
		{
			// Disconnect
			disconnect(resultSet, statement, connection);
		}

		return false;
	}

	/**
	 * Executes a query and returns the ResultSet
	 *
	 * @param query
	 * @return
	 */
	@Override
	public ResultSet executeQuery(String query)
	{
		ResultSet resultSet = null;
		Statement statement = null;
		Connection connection = null;

		try
		{
			// Connect and execute query
			connection = connect();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);
		}
		catch (SQLException ex)
		{
			SQLExceptionHandler.printSQLiteExceptionMessage(ex);
		}
		finally
		{
			disconnect(resultSet, statement, connection);
		}

		// Return resultset
		return resultSet;
	}

	/**
	 * Executes an query action and returns a boolean
	 *
	 * @param query
	 * @return
	 */
	@Override
	public boolean executeAction(String query)
	{
		ResultSet resultSet = null;
		Statement statement = null;
		Connection connection = null;

		try
		{
			// Connect and execute action
			connection = connect();
			statement = connection.createStatement();
			statement.execute(query);
			return true;
		}
		catch (SQLException ex)
		{
			SQLExceptionHandler.printSQLiteExceptionMessage(ex);
		}
		finally
		{
			// Disconnect
			disconnect(resultSet, statement, connection);
		}

		// Failed to execute action
		return false;
	}

	/**
	 * When we run this app, we want to populate current weeks shows.
	 */
	public void populateWeeklyShows()
	{
		// Fetch from remote
		List<ShowSchedule> showSchedules = TVMazeApi.getInstance().getWeeklyShowSchedules();

		// Insert into Database 
		// (if not already done so - will do a quick check and skip the ones already in DB)
		insertWeeklyShowSchedules(showSchedules);
	}

	/**
	 * Populates all shows, by iterating through index
	 *
	 * FIXME: Currently we only iterate though the first index for test
	 * purposes, where index=0.
	 *
	 * @param loadEpisodes
	 */
	public void populateShows(boolean loadEpisodes)
	{
		DatabaseConfig dbConfigInstance = DatabaseConfig.getInstance();

		// Get first (0 <= index <= 250) shows
		List<Show> shows = TVMazeApi.getInstance().getShowByIndex(dbConfigInstance.getIndex(), loadEpisodes); // Don't load episodes until we need to display them.

		// FIXME: Load more than just the first index=0.
		while (dbConfigInstance.getIndex() == 0)
		{
			// Iterate through each episode
			shows.stream().forEach((Show show) ->
			{
				// Get network
				Network network = show.getNetwork();

				// Insert Network into database
				insertNetwork(network);

				// Insert Show into Database
				insertShow(show);

				// Store schedule to Days and Time table
				List<String> days = show.getSchedule().getDays();
				if (!days.isEmpty())
				{
					for (String day : days)
					{
						insertDay(show.getId(), day);
					}
				}

				// Attempt to get Time and store it in Database
				try
				{
					Time time = Time.valueOf(show.getSchedule().getTime());
					insertTime(show.getId(), time);
				}
				catch (Exception ex)
				{
					System.err.println("Exception (" + ex.getClass().getName() + "):\n" + ex.getMessage());
				}

				// insertTime(show.getId(), time);
				// Iterate through each episode
				show.getEpisodes().stream().forEach((Episode episode) ->
				{
					// Insert episode into database
					insertEpisode(episode);
				});
			});

			// Increment for next iteration	
			dbConfigInstance.incrementIndex();

			// Save changes
			dbConfigInstance.save();
		}
	}

	/**
	 * Populates the database with 250 shows (without episodes).
	 *
	 * We keep a config file to keep track of how far we have added data from a
	 * remote index. For the sake of this demo, I have limited this to only get
	 * the first (index=0) index.
	 *
	 * During a test, it took almost 30 minutes to load all episodes and shows
	 * for only 250 shows.
	 *
	 * Takes about ~20-40 seconds to populate the shows without the episodes
	 *
	 * @return
	 */
	@Override
	public boolean populateDatabase()
	{
		// Populate weekly shows
		populateWeeklyShows();

		// Populate shows (without episodes)
		populateShows(false);

		return true;
	}

	/**
	 * Insert a Day entry
	 *
	 * @param showId
	 * @param day
	 * @return
	 */
	private boolean insertDay(int showId, String day)
	{
		// Trim day in case there are trailing whitespace
		day = day.trim();

		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		Connection connection = null;

		try
		{
			// Get connection
			connection = connect();

			// Insert into database
			String query = "INSERT INTO " + DaysEntry.TABLE_NAME + "("
					+ DaysEntry.COLUMN_SHOW_ID + ", "
					+ DaysEntry.COLUMN_DAY + ") values (?, ?)";
			preparedStatement = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, showId);
			preparedStatement.setString(2, day);
			preparedStatement.executeUpdate();

			// Get resultset
			resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next())
			{
				return true;
			}
		}
		catch (SQLException ex)
		{
			SQLExceptionHandler.printSQLiteExceptionMessage(ex);
		}
		catch (Exception ex)
		{
			System.out.println("Exception (" + ex.getClass().getName() + "):\n" + ex.getMessage());
		}
		finally
		{
			disconnect(resultSet, preparedStatement, connection);
		}

		return false;
	}

	/**
	 * Insert Time Entry
	 *
	 * @param showId
	 * @param time
	 * @return
	 */
	public boolean insertTime(int showId, Time time)
	{
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		Connection connection = null;

		try
		{
			// Get connection
			connection = connect();

			// Insert into database
			String query = "INSERT INTO " + TimeEntry.TABLE_NAME + "("
					+ TimeEntry.COLUMN_SHOW_ID + ", "
					+ TimeEntry.COLUMN_TIME + ") values (?, ?)";
			preparedStatement = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, showId);
			preparedStatement.setTime(2, time);
			preparedStatement.executeUpdate();

			// Get resultset
			resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next())
			{
				return true;
			}
		}
		catch (SQLException ex)
		{
			SQLExceptionHandler.printSQLiteExceptionMessage(ex);
		}
		catch (Exception ex)
		{
			System.out.println("Exception (" + ex.getClass().getName() + "):\n" + ex.getMessage());
		}
		finally
		{
			disconnect(resultSet, preparedStatement, connection);
		}

		return false;
	}

	/**
	 * Insert a Network into Database
	 *
	 * @param network
	 * @return
	 */
	@Override
	public Network insertNetwork(Network network)
	{
		// If the ID is -1, it is invalid (empty or unfinished)
		if (network.getId() == -1)
		{
			return null;
		}

		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		Connection connection = null;

		try
		{
			// Get connection
			connection = connect();

			// Insert into database
			String query = "INSERT INTO " + NetworkEntry.TABLE_NAME + "("
					+ NetworkEntry.COLUMN_ID + ", "
					+ NetworkEntry.COLUMN_NAME + ", "
					+ NetworkEntry.COLUMN_COUNTRY + ") values (?, ?, ?)";
			preparedStatement = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, network.getId());
			preparedStatement.setString(2, network.getName());
			preparedStatement.setString(3, network.getCountryJson());
			preparedStatement.executeUpdate();

			// Get resultset
			resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next())
			{
				return network;
			}
		}
		catch (SQLException ex)
		{
			SQLExceptionHandler.printSQLiteExceptionMessage(ex);
		}
		finally
		{
			disconnect(resultSet, preparedStatement, connection);
		}

		return null;
	}

	@Override
	public boolean updateNetwork(Network network)
	{
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public boolean deleteNetwork(int networkId)
	{
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Network getNetwork(int networkId)
	{
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public List<Network> getAllNetworks()
	{
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public List<Show> getAllShowsWithNetworkId(int networkId)
	{
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Show insertShow(Show show)
	{
		// If the ID is -1, it is invalid (empty, unfinished)
		if (show.getId() == -1)
		{
			return null;
		}

		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		Connection connection = null;

		try
		{
			// Get connection
			connection = connect();

			// Insert into database
			String query = "INSERT INTO " + ShowEntry.TABLE_NAME + "("
					+ ShowEntry.COLUMN_ID + ", "
					+ ShowEntry.COLUMN_NAME + ", "
					+ ShowEntry.COLUMN_RATING + ", "
					+ ShowEntry.COLUMN_STATUS + ", "
					+ ShowEntry.COLUMN_JSON_CONTENT + ", "
					+ ShowEntry.COLUMN_NETWORK_ID + ") values (?, ?, ?, ?, ?, ?)";
			preparedStatement = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, show.getId());
			preparedStatement.setString(2, show.getName());
			preparedStatement.setFloat(3, show.getRating());
			preparedStatement.setString(4, show.getStatus());
			preparedStatement.setString(5, show.toJson());

			// Handle Show without a Network
			if (show.getNetwork().getId() != -1)
			{
				preparedStatement.setInt(6, show.getNetwork().getId());
			}
			else
			{
				preparedStatement.setNull(6, java.sql.Types.INTEGER);
			}

			preparedStatement.executeUpdate();

			// Get resultset
			resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next())
			{
				// All is good, return
				return show;
			}
		}
		catch (SQLException ex)
		{
			SQLExceptionHandler.printSQLiteExceptionMessage(ex);
		}
		finally
		{
			// Disconnect
			disconnect(resultSet, preparedStatement, connection);
		}

		return null;
	}

	/**
	 * Ivan's Note @ 13:13, 17.09.17
	 *
	 *
	 * Just learned about Batch inserts. Testing it now in hope that it will
	 * increase speed.
	 *
	 * @param showSchedules
	 * @return
	 */
	@Override
	public boolean insertWeeklyShowSchedules(List<ShowSchedule> showSchedules)
	{
		// For each daily episodes
		for (ShowSchedule showSchedule : showSchedules)
		{
			// Add show (if not already added)
			insertShow(showSchedule.getShow());

			// Insert each episode (if not already added)
			for (Episode episode : showSchedule.getEpisodeDays())
			{
				try
				{
					episode.setShowId(showSchedule.getShowId());
					insertEpisode(episode);
				}
				catch (Exception ex)
				{
					// "Don't stop until you get enough"
				}
			}
		}

		return true;
	}

	/**
	 * Not needed for this demo
	 */
	@Override
	public boolean updateShow(Show show)
	{
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	/**
	 * Not needed for this demo
	 */
	@Override
	public boolean deleteShow(int showId)
	{
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Show getShow(int showId)
	{
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public List<Show> getAllShows()
	{
		ResultSet resultSet = null;
		Statement statement = null;
		Connection connection = null;

		List<Show> shows = new ArrayList<>();

		try
		{
			// Get connection
			connection = connect();

			// Insert into database
			String query = "SELECT * FROM " + ShowEntry.TABLE_NAME;
			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);

			while (resultSet.next())
			{
				try
				{
					Show show = GSON.fromJson(resultSet.getString(ShowEntry.COLUMN_JSON_CONTENT), Show.class);
					System.out.println("Show's Episode? " + show.getEpisodes());
					shows.add(show);
				}
				catch (JsonSyntaxException ex)
				{
					ex.printStackTrace();
				}
			}
		}
		catch (SQLException ex)
		{
			SQLExceptionHandler.printSQLiteExceptionMessage(ex);
		}
		finally
		{
			// Disconnect
			disconnect(resultSet, statement, connection);
		}

		return shows;
	}

	/**
	 * This will return a List<Show> that contains all shows that are running
	 * CURRENT week.
	 *
	 * @return
	 */
	@Override
	public List<ShowSchedule> getWeeklyShowSchedules()
	{
//		ResultSet resultSet = null;
//		Statement statement = null;
//		Connection connection = null;
//
//		// Get currentWeek Start and End
//		Calendar calendar = GregorianCalendar.getInstance();
//		calendar.setFirstDayOfWeek(Calendar.MONDAY); // Start with Monday
//
//		// Set to Monday
//		calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
//		String monday = calendar.getTime() + "";
//
//		// Set to Sunday
//		calendar.add(Calendar.DATE, 6);
//		String sunday = calendar.getTime() + "";
//
//		try
//		{
//			// Get connection
//			connection = connect();
//
//			String query = "SELECT * "
//					+ "FROM episode E "
//					+ "WHERE E.show_id "
//					+ "IN ("
//					+ "SELECT S.id "
//					+ "FROM show S "
//					+ "WHERE strftime('%Y-%m-%d', E.airdate / 1000, 'unixepoch') "
//					+ "BETWEEN date('" + monday + "') AND date('" + sunday + "')"
//					+ ")";
//
//			// String query = "SELECT * FROM show A INNER JOIN days B ON A.id = B.show_id WHERE B.day = 'Wednesday'";
//			statement = connection.createStatement();
//			resultSet = statement.executeQuery(query);
//
//			// Get all episodes
//			List<Episode> episodes = new ArrayList<>();
//
//			// Get all shows (which include Episodes)
//			while (resultSet.next())
//			{
//				// Get Json Content
//				String jsonContent = resultSet.getString(ShowEntry.COLUMN_JSON_CONTENT);
//
//				// Create Show object
//				Episode episode = GSON.fromJson(jsonContent, Episode.class);
//
//				// Append Episode
//				episodes.add(episode);
//			}
//
//			// ---- Setting up List<ShowSchedule> ----
//			// Show ID, ShowSchedule Map
//			Map<Integer, ShowSchedule> showSchedules = new HashMap<>();
//
//			// Get shows that run this week
//			for (int day = 0; day < 7; day++)
//			{
//				// Get the episodes for the day
//				List<Episode> dailyEpisodes = getEpisodesByDate.get(day); // 0 = Monday, 1 = Tuesday, (...)
//
//				// Episodes for Day [#]
//				for (Episode episode : dailyEpisodes)
//				{
//					Show show = episode.getShow();
//					int showId = show.getId();
//
//					// Store the Show in our ShowSchedule HashMap (if not already added)
//					if (!showSchedules.containsKey(showId))
//					{
//						// Add new Show with schedule
//						showSchedules.put(showId, new ShowSchedule(show));
//					}
//
//					try
//					{
//						// Add schedule
//						showSchedules.get(showId).addDay(day, episode);
//					}
//					catch (NullPointerException ex)
//					{
//						// Not defined in Json, add empty episode
//						showSchedules.get(showId).addDay(day, new Episode());
//					}
//				}
//			}
//
//			// Sorted listing
//			List<ShowSchedule> sortedSchedule = new ArrayList<>(showSchedules.values());
//			Collections.sort(sortedSchedule);
//
//			// Return List<ShowSchedule>
//			return sortedSchedule;
//
//		}
//		catch (SQLException ex)
//		{
//			SQLExceptionHandler.printSQLiteExceptionMessage(ex);
//		}
//		finally
//		{
//			// Disconnect
//			disconnect(resultSet, null, null);
//		}
		return null;
	}

	public List<Episode> getEpisodesByDate(Date date)
	{
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		Connection connection = null;

		List<Episode> episodesByDate = new ArrayList<>();
		try
		{
			// Format string
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String formattedDate = dateFormat.format(date);

			// Get connection
			connection = connect();

			// Insert into database
			String query = "SELECT * FROM " + EpisodeEntry.TABLE_NAME + " E "
					+ "WHERE strftime('%Y-%m-%d', E.airdate / 1000, 'unixepoch') = date('" + formattedDate + "')";
			preparedStatement = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
			preparedStatement.executeUpdate();

			// Get resultset
			resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next())
			{
				try
				{
					Episode episode = GSON.fromJson(resultSet.getString(EpisodeEntry.COLUMN_JSON_CONTENT), Episode.class);
					episodesByDate.add(episode);
				}
				catch (JsonSyntaxException ex)
				{
					ex.printStackTrace();
				}
			}
		}
		catch (SQLException ex)
		{
			SQLExceptionHandler.printSQLiteExceptionMessage(ex);
		}

		return episodesByDate;
	}

	/**
	 * Returns all Episodes by date
	 *
	 * @param date
	 * @return
	 */
//	public List<Episode> getEpisodesAndShowSchedulesByDate(java.util.Date date)
//	{
//		// Format string
//		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//		String formattedDate = dateFormat.format(date);
//
//		// Get Json content
//		// Get content type as List<Episode>
//		Type collectionType = new TypeToken<Collection<Episode>>()
//		{
//		}.getType();
//		List<Episode> episodes = GSON.fromJson(jsonContent, collectionType);
//
//		// Return List<Episode>
//		return episodes;
//	}
	/**
	 * Used to insert an Episode
	 *
	 * @param episode
	 * @return
	 */
	@Override
	public Episode insertEpisode(Episode episode)
	{
		// If the ID is -1, it is invalid (empty, unfinished)
		if (episode.getId() == -1)
		{
			return null;
		}

		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		Connection connection = null;

		try
		{
			// Get connection
			connection = connect();

			// Insert into database
			String query = "INSERT INTO " + EpisodeEntry.TABLE_NAME + "("
					+ EpisodeEntry.COLUMN_ID + ", "
					+ EpisodeEntry.COLUMN_SHOW_ID + ", "
					+ EpisodeEntry.COLUMN_NAME + ", "
					+ EpisodeEntry.COLUMN_SEASON + ", "
					+ EpisodeEntry.COLUMN_NUMBER + ", "
					+ EpisodeEntry.COLUMN_AIRDATE + ", "
					+ EpisodeEntry.COLUMN_AIRTIME + ", "
					+ EpisodeEntry.COLUMN_JSON_CONTENT + ") values (?, ?, ?, ?, ?, ?, ?, ?)";
			preparedStatement = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, episode.getId());
			preparedStatement.setInt(2, episode.getShowId());
			preparedStatement.setString(3, episode.getName());
			preparedStatement.setInt(4, episode.getSeason());
			preparedStatement.setInt(5, episode.getNumber());

			if (episode.getAirDate() != null)
			{
				preparedStatement.setDate(6, Date.valueOf(episode.getAirDate()));
			}
			else
			{
				preparedStatement.setNull(6, java.sql.Types.DATE);
			}

			if (episode.getAirTime() != null)
			{
				preparedStatement.setTime(7, Time.valueOf(episode.getAirTime()));
			}
			else
			{
				preparedStatement.setNull(7, java.sql.Types.DATE);
			}

			preparedStatement.setString(8, episode.toJson());

			// Excecute
			preparedStatement.executeUpdate();

			// Get resultset
			resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next())
			{
				// All is good, return
				return episode;
			}
		}
		catch (SQLException ex)
		{
			SQLExceptionHandler.printSQLiteExceptionMessage(ex);
		}
		finally
		{
			// Disconnect
			disconnect(resultSet, preparedStatement, connection);
		}

		return null;
	}

	@Override
	public boolean updateEpisode(Episode episode)
	{
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public boolean deleteEpisode(int episodeId)
	{
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Episode getEpisode(int episodeId)
	{
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public List<Episode> getAllEpisodes()
	{
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public List<Episode> getAllEpisodesWithShowId(int showId)
	{
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

}
