/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivanskodje.tvmazeapp.report;

import java.util.List;

/**
 *
 * @author Ivan Skodje
 * @param <T>
 */
public interface Reporter<T>
{

	/* Formatter using generic lists */
	String format(List<T> items);
}
