/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivanskodje.app.database;

import java.sql.SQLException;
import org.sqlite.SQLiteErrorCode;

/**
 *
 * @author Ivan Skodje
 */
public class SQLExceptionHandler
{

	private SQLExceptionHandler()
	{
		// Nothing interesting ever happens here :(
	}

	/**
	 * Prints informative SQLException messages according to the error code
	 * (from SQLite).
	 *
	 * SQLite's Error Code documentation: https://sqlite.org/rescode.html
	 *
	 * @param ex
	 * @return
	 */
	public static void printSQLiteExceptionMessage(SQLException ex)
	{
		// Get SQLite Error Code
		SQLiteErrorCode errorCode = SQLiteErrorCode.getErrorCode(ex.getErrorCode());

		/**
		 * Ivan's Dev Note:
		 *
		 * Depending on what we want to do, we may want to take certain actions
		 * to react on certain errors. However for this demo, I will refrain
		 * from making such implementations due to time constraints.
		 */
		// Handle errors
		switch (errorCode)
		{
			case SQLITE_CONSTRAINT:
				System.err.println("SQLite Error @ [" + ex.getClass().getName() + "], Aborted due to a constraint violation:\n" + ex.getMessage());
				break;
			case SQLITE_ERROR:
			default:
				System.err.println("SQLite Error @ [" + ex.getClass().getName() + "], " + errorCode.message + ":\n" + ex.getMessage());
		}
	}
}
