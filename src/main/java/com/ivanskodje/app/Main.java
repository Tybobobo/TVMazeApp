package com.ivanskodje.app;

import com.ivanskodje.app.helpers.StageHelper;
import com.ivanskodje.app.preloader.AppPreloader;
import com.sun.javafx.application.LauncherImpl;
import java.util.Locale;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application
{

	/**
	 * The idea is to avoid pulling in huge amounts of information at once, and
	 * in instead retrieve information on demand.
	 */
	/* App Name */
	private final String app_name = "TVMaze Java App";

	/* Preload */
	@Override
	public void init() throws Exception
	{
		// TODO: Load Locale from config
		Locale.setDefault(new Locale("en", "US"));
	}

	/**
	 * The main entry point for all JavaFX applications.
	 *
	 * This was made with help from various github repositories, API
	 * documentation and stackoverflow.
	 *
	 * @param stage
	 * @throws Exception
	 */
	@Override
	public void start(Stage stage) throws Exception
	{
		// Load FXML
		Parent root = FXMLLoader.load(getClass().getResource("/fxml/Main.fxml"));

		// Create scene, add FXML with a stylesheet
		Scene scene = new Scene(root);
		scene.getStylesheets().add("/styles/Styles.css");

		// Setup icons
		StageHelper.setupIcons(stage);

		// Set scene, properties and show window
		stage.setTitle(app_name);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * The main() method is ignored in correctly deployed JavaFX application.
	 * main() serves only as fallback in case the application can not be
	 * launched through deployment artifacts, e.g., in IDEs with limited FX
	 * support. NetBeans ignores main().
	 *
	 * @param args
	 */
	public static void main(String[] args)
	{
		LauncherImpl.launchApplication(Main.class, AppPreloader.class, args);
	}

}
