/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivanskodje.tvmazeapp.model;

import com.google.gson.Gson;

/**
 *
 * @author Ivan Skodje
 */
public class Network implements Comparable<Network>
{

	// Gson is practically a multi-tool
	private static final Gson GSON = new Gson();

	private Integer id;
	private String name;
	private Country country;

	public int getId()
	{
		if (id == null)
		{
			id = -1;
		}
		return id;
	}

	public String getName()
	{
		if (name == null)
		{
			name = "";
		}
		return name;
	}

	public Country getCountry()
	{
		if (country == null)
		{
			country = new Country();
		}
		return country;
	}

	public String getCountryJson()
	{
		return GSON.toJson(getCountry());
	}

	@Override
	public int compareTo(Network o)
	{
		return getName().replaceAll("(?i)^the\\s", "").compareTo(o.getName().replaceAll("(?i)^the\\s", ""));
	}

	@Override
	public String toString()
	{
		return getName();
	}
}
