/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivanskodje.app.database.tables;

/**
 *
 * @author Ivan Skodje
 */
public final class ShowEntry
{

	// Table name
	public static final String TABLE_NAME = "show";

	// Table id
	public static final String COLUMN_ID = "id";

	// Table name
	public static final String COLUMN_NAME = "name";

	// Table rating
	public static final String COLUMN_RATING = "rating"; // Used to get top 10, etc.

	// Table status
	public static final String COLUMN_STATUS = "status";

	// Table network ID
	public static final String COLUMN_NETWORK_ID = "network_id";

	// Table json content : Used to retrieve data we may not always want to load.
	public static final String COLUMN_JSON_CONTENT = "json_content";

	// Table date created
	public static final String COLUMN_DATE_CREATED = "date_created";
}
