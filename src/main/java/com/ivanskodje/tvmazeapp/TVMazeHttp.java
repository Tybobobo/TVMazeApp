/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivanskodje.tvmazeapp;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.yamj.api.common.exception.ApiException;
import org.yamj.api.common.exception.ApiExceptionType;
import org.yamj.api.common.http.DigestedResponse;
import org.yamj.api.common.http.DigestedResponseReader;
import org.yamj.api.common.http.SimpleHttpClientBuilder;

/**
 * TVMazeHttp is a Fascade for interacting with the TVMaze API.
 * http://www.tvmaze.com/api
 *
 * @author Ivan Skodje
 */
public class TVMazeHttp
{

	// HTTPClient
	private final HttpClient httpClient;

	// HTTP Status
	private static final int HTTP_MULTIPLE_CHOICES = 300;
	private static final int HTTP_NOT_FOUND = 404;			// No page found (Ex: no matching search result)
	private static final int HTTP_RATE_EXCEEDED = 429;		// API Calls limited to 20 calls every 10 seconds
	private static final int HTTP_GENERAL_ERROR = 500;		// Mysterious errors that only a wizard can solve

	// 429 Max Wait Time
	private static final int MAX_WAIT_TIME = 16;			// A safety measure to prevent infinite call attempts

	// Charset
	private final Charset charset;
	private static final String DEFAULT_CHARSET = "UTF-8";

	/**
	 * TVMazeApi Constructor is used to separate TVMaze API with the rest of the
	 * project code.
	 *
	 * @throws ApiException
	 */
	public TVMazeHttp() throws ApiException
	{
		this(new SimpleHttpClientBuilder().build());
	}

	public TVMazeHttp(HttpClient httpClient)
	{
		this.httpClient = httpClient;
		this.charset = Charset.forName(DEFAULT_CHARSET);
	}

	/**
	 * Using one or more search queries and an apiUrlm we send a request to URL
	 * and attempt to retrieve the data.
	 *
	 * @param apiQueries
	 * @param apiUrl
	 * @return
	 */
	public DigestedResponse getPage(List<String> apiQueries, String apiUrl)
	{
		try
		{
			// Wait time for when request rate is exceeded (HTTP Status 429)
			int waitTimeSeconds = 1;
			boolean rateExceeded = false;

			// Response
			DigestedResponse response;

			// If queries is null, create an empty array.
			if (apiQueries == null)
			{
				apiQueries = new ArrayList<>();
			}

			// Since we may run getPage multiple times, we want to avoid directly manipulating the apiQueries reference. 
			// Since we do not expect more than 1-3 parameters, 
			// this will not affect performance at all.
			List<String> queries = new ArrayList<>(apiQueries);

			// Replace spaces and + with %20 in each query
			for (int i = 0; i < queries.size(); i++)
			{
				queries.set(i, java.net.URLEncoder.encode(queries.get(i), "UTF-8"));
			}

			// Full URL path
			String fullUrl = String.format(apiUrl, queries.toArray());

			// Crate URL from full path
			URL url = new URL(fullUrl);

			// Create request
			HttpGet request = new HttpGet(url.toURI());

			do
			{
				/**
				 * If rateExceeded is set to true, it means we have an HTTP
				 * Status 429 and need to wait a bit before trying to fetch
				 * page.
				 *
				 * In order to avoid requesting pages too quickly, we double the
				 * wait time each time we fail.
				 */
				if (rateExceeded)
				{
					// Wait
					TimeUnit.SECONDS.sleep(waitTimeSeconds);

					// Double waitTime if we need to wait again.
					waitTimeSeconds *= 2;

					System.err.println("HTTP_RATE_EXCEEDED IS NOW TRUE! WAITING FOR " + waitTimeSeconds + " SECONDS...!");

					// If our set wait time have exceeded a set amount, cancel attempt to call.
					if (waitTimeSeconds > MAX_WAIT_TIME)
					{
						throw new Exception("We are unable to get the TVMaze page you requested. Please try again at a later time.");
					}

					// Set to false until told otherwise.
					rateExceeded = false;
				}

				// Request Content and insert into a Response (yamj)
				response = DigestedResponseReader.requestContent(httpClient, request, charset);

				// Check HTTP Status code
				switch (response.getStatusCode())
				{
					case HTTP_GENERAL_ERROR:
						throw new ApiException(ApiExceptionType.HTTP_503_ERROR, response.getContent(), response.getStatusCode(), url);
					case HTTP_MULTIPLE_CHOICES:
					case HTTP_NOT_FOUND:
						throw new ApiException(ApiExceptionType.HTTP_404_ERROR, response.getContent(), response.getStatusCode(), url);
					case HTTP_RATE_EXCEEDED:
						rateExceeded = true;
					default:
						break;
				}
			}
			while (rateExceeded);

			// Return site content
			return response;
		}

		/**
		 * Thrown when an application attempts to use null in a case where an
		 * object is required.
		 *
		 * Exception -> RuntimeException -> NullPointerException
		 */
		catch (NullPointerException ex)
		{
			System.err.println("--- NullPointerException: apiURL cannot be null. \n" + ex.getMessage());
		}

		/**
		 * Thrown to indicate that a malformed URL has occurred. Either no legal
		 * protocol could be found in a specification string or the string could
		 * not be parsed
		 *
		 * Exception -> IOException -> MalformedURLException
		 */
		catch (MalformedURLException ex)
		{
			System.err.println("MalformedURLException: URL is not using correct format. \n" + ex.getMessage());
		}

		/**
		 * Signals that an I/O exception of some sort has occurred. This class
		 * is the general class of exceptions produced by failed or interrupted
		 * I/O operations.
		 *
		 * Exception -> IOException
		 */
		catch (IOException ex)
		{
			System.err.println("IOException: " + ex.getMessage());
		}

		/**
		 * Checked exception thrown to indicate that a string could not be
		 * parsed as a URI reference
		 *
		 * Exception -> URISyntaxException
		 */
		catch (URISyntaxException ex)
		{
			System.err.println("URISyntaxException: Error connecting to URL. \n" + ex.getMessage());
		}

		/**
		 * Thrown when a thread is waiting, sleeping, or otherwise occupied, and
		 * the thread is interrupted, either before or during the activity.
		 *
		 * Exception -> InterruptedException
		 */
		catch (InterruptedException ex)
		{
			System.err.println("InterruptedException: Failed to request page before interruption.\n" + ex.getMessage());
		}

		/**
		 * API Exception from org.yamj.api.common.exception
		 */
		catch (ApiException ex)
		{
			System.err.println("ApiException: Failed to create URL.\n" + ex.getMessage());
		}

		/**
		 * The class Exception and its subclasses are a form of Throwable that
		 * indicates conditions that a reasonable application might want to
		 * catch.
		 *
		 * Exception
		 */
		catch (Exception ex)
		{
			System.err.println("Exception:\n" + ex.getMessage());
		}

		// Since we failed to return before this point; Return null
		return null;
	}

	/**
	 * TESTING Method.
	 *
	 * Since I have been unable to get any HTTP Status 429 from the web-site
	 * (even when calling over 2000 times within seconds!), so this method is
	 * used to manually create false 429 statuses. Not very clean, but it is
	 * functional and do test the algorithm used in the real method.
	 *
	 * We use numberOfFails to determine how many times we will test and get a
	 * HTTP Status 429.
	 *
	 * @param apiQueries
	 * @param apiUrl
	 * @param numberOfFails
	 * @return
	 * @throws ApiException
	 */
	DigestedResponse testGetPage429Status(List<String> apiQueries, String apiUrl, int numberOfFails, int maxWaitTimeBeforeCancel) throws Exception
	{
		// Since we may run getPage multiple times, we want to avoid directly manipulating the apiQueries reference. 
		// Since we do not expect more than 1-3 parameters, 
		// this will not affect performance at all.
		List<String> queries = new ArrayList<>(apiQueries);

		try
		{
			// Wait time for when request rate is exceeded (HTTP Status 429)
			int failCounter = 0;
			int waitTimeSeconds = 1;
			boolean rateExceeded = true;

			// Response
			DigestedResponse response;

			// Replace spaces and + with %20 in each query
			for (int i = 0; i < queries.size(); i++)
			{
				queries.set(i, java.net.URLEncoder.encode(queries.get(i), "UTF-8"));
			}

			// Full URL path
			String fullUrl = String.format(apiUrl, queries);

			// Crate URL from full path
			URL url = new URL(fullUrl);

			// Create request
			HttpGet request = new HttpGet(url.toURI());

			do
			{
				/**
				 * If rateExceeded is set to true, it means we have an HTTP
				 * Status 429 and need to wait a bit before trying to fetch
				 * page.
				 *
				 * In order to avoid requesting pages too quickly, we double the
				 * wait time each time we fail.
				 *
				 * Wait Time: 1 -> 2 -> 4 -> 8 -> 16 -> (...)
				 */
				if (rateExceeded)
				{
					System.out.println("--- Waiting " + waitTimeSeconds + " seconds... ");
					// Wait
					TimeUnit.SECONDS.sleep(waitTimeSeconds);

					// Double waitTime if we need to wait again.
					waitTimeSeconds *= 2;

					// If our set wait time have exceeded a set amount, stop attempinting to call TVMaze
					if (waitTimeSeconds > maxWaitTimeBeforeCancel)
					{
						throw new Exception("We are unable to get the TVMaze page you requested. Please try again at a later time.");
					}

					// Increment fail counter
					failCounter += 1;

					// Stop failing if failCounter is greater than numberOfFails
					if (failCounter > numberOfFails)
					{
						// Set to false until told otherwise.
						rateExceeded = false;
					}

				}

				// Request Content and insert into a Response (yamj)
				response = DigestedResponseReader.requestContent(httpClient, request, charset);

				// Check HTTP Status code
				switch (response.getStatusCode())
				{
					case HTTP_GENERAL_ERROR:
						throw new ApiException(ApiExceptionType.HTTP_503_ERROR, response.getContent(), response.getStatusCode(), url);
					case HTTP_MULTIPLE_CHOICES:
					case HTTP_NOT_FOUND:
						throw new ApiException(ApiExceptionType.HTTP_404_ERROR, response.getContent(), response.getStatusCode(), url);
					case HTTP_RATE_EXCEEDED:
						rateExceeded = true;
					default:
						System.out.println(rateExceeded ? "STATUS CODE: (Manual) 429" : "STATUS CODE: " + response.getStatusCode());
				}
			}
			while (rateExceeded);

			// Return site content
			return response;
		}

		catch (Exception ex)
		{
			System.err.println("Exception in Test Method: \n" + ex.getMessage());
		}

		// Since we failed to return before this point; Return null
		return null;
	}
}
