/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivanskodje.tvmazeapp.model;

import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Show.class is serialized and deserialised using Gson for ease of use. The
 * same applies for Network, Country, Schedule and Episode.
 *
 * @author Ivan Skodje
 */
public class Show implements Comparable<Show>
{

	// Gson is life, Gson is happiness
	private static final Gson GSON = new Gson();

	// ID
	private int id;

	// URL to the Show page
	private String url;

	// Name
	private String name;

	// Show type
	private String type;

	// Spoken language
	private String language;

	// Genres
	private List<String> genres;

	// Show status : Ended, Ongoing, ...
	private String status;

	// Runtime per episode
	private int runtime;

	// Premiere date
	private Date premiered;

	// Official web-site for the show
	private String officialSite;

	// Show airing schedule
	private Schedule schedule;

	// Show rating(s)
	private Map<String, Float> rating;

	// Weight
	private int weight;

	// Network Details the Show runs on
	private Network network;

	// Externals contain IMDB Number
	private Externals externals;

	// Image URLs
	private Map<String, String> image;

	// Updated 
	private int updated;

	// Show summary : A short description of the show
	private String summary;

	// Links
	private Links _links;

	// All Episodes
	private List<Episode> episodes;

	// Last Aired Episode
	private Episode previousEpisode;

	// Embedded properties such as Episodes
	private Embedded _embedded;

	/**
	 * Get/Set show ID
	 *
	 * @return
	 */
	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	/**
	 * Get/Set URL
	 *
	 * @return
	 */
	public String getUrl()
	{
		return url;
	}

	public void setUrl(String url)
	{
		this.url = url;
	}

	/**
	 * Get/Set name
	 *
	 * @return
	 */
	public String getName()
	{
		if (name == null)
		{
			name = "";
		}
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * Get/Set Type
	 *
	 * @return
	 */
	public String getType()
	{
		if (type == null)
		{
			type = "";
		}
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
	}

	/**
	 * Get/Set Language
	 *
	 * @return
	 */
	public String getLanguage()
	{
		if (language == null)
		{
			language = "";
		}
		return language;
	}

	public void setLanguage(String language)
	{
		this.language = language;
	}

	/**
	 * Get/Set Genres
	 *
	 * @return
	 */
	public List<String> getGenres()
	{
		if (genres == null)
		{
			genres = new ArrayList<>();
		}
		return genres;
	}

	public void setGenres(List<String> genres)
	{
		this.genres = genres;
	}

	/**
	 * Get/Set show status
	 *
	 * @return
	 */
	public String getStatus()
	{
		if (status == null)
		{
			status = "";
		}
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	/**
	 * Get/Set Runtime
	 *
	 * @return
	 */
	public int getRuntime()
	{
		return runtime;
	}

	public void setRuntime(int runtime)
	{
		this.runtime = runtime;
	}

	/**
	 * Get/Set date premiered
	 *
	 * @return
	 */
	public Date getPremiered()
	{
		if (premiered == null)
		{
			premiered = new Date();
		}
		return premiered;
	}

	public void setPremiered(Date premiered)
	{
		this.premiered = premiered;
	}

	/**
	 * Get/Set official web-site URL
	 *
	 * @return
	 */
	public String getOfficialSite()
	{
		if (officialSite == null)
		{
			officialSite = "";
		}
		return officialSite;
	}

	public void setOfficialSite(String officialSite)
	{
		this.officialSite = officialSite;
	}

	/**
	 * Get/Set the airing schedule of the show
	 *
	 * @return
	 */
	public Schedule getSchedule()
	{
		if (schedule == null)
		{
			schedule = new Schedule();
		}
		return schedule;
	}

	public void setSchedule(Schedule schedule)
	{
		this.schedule = schedule;
	}

	/**
	 * Get/Set rating (float)
	 *
	 * @return
	 */
	public float getRating()
	{
		try
		{
			if (rating != null && rating.containsKey("average"))
			{
				return rating.get("average");
			}
		}
		catch (Exception ex)
		{
			System.err.println("Exception (" + ex.getClass().getName() + "):\n" + ex.getMessage());
		}

		return -1.0f;
	}

	/**
	 * Get/Set weight
	 *
	 * @return
	 */
	public int getWeight()
	{
		return weight;
	}

	public void setWeight(int weight)
	{
		this.weight = weight;
	}

	/**
	 * Get/Set the network the show is run on
	 *
	 * @return
	 */
	public Network getNetwork()
	{
		if (network == null)
		{
			network = new Network();
		}
		return network;
	}

	public void setNetwork(Network network)
	{
		this.network = network;
	}

	/**
	 * Get/Set externals
	 *
	 * @return
	 */
	public Externals getExternals()
	{
		if (externals == null)
		{
			externals = new Externals();
		}
		return externals;
	}

	public void setExternals(Externals externals)
	{
		this.externals = externals;
	}

	/**
	 * Get/Set images of the show
	 *
	 * @return
	 */
	public Map<String, String> getImage()
	{
		if (image == null)
		{
			image = new HashMap<String, String>();
		}
		return image;
	}

	/**
	 * Get/Set updated
	 *
	 * @return
	 */
	public int getUpdated()
	{
		return updated;
	}

	public void setUpdated(int updated)
	{
		this.updated = updated;
	}

	/**
	 * Get/Set show summary
	 *
	 * @return
	 */
	public String getSummary()
	{
		if (summary == null)
		{
			summary = "";
		}
		return summary;
	}

	public void setSummary(String summary)
	{
		this.summary = summary;
	}

	/**
	 * Get/Set links, which contains link to self and latest released episode.
	 *
	 * @return
	 */
	public Links getLinks()
	{
		if (_links == null)
		{
			_links = new Links();
		}
		return _links;
	}

	public void setLinks(Links _links)
	{
		this._links = _links;
	}

	/**
	 * Get/Set all episodes for the show
	 *
	 * TODO: Fix serialization so that we get Episodes from _embedded
	 *
	 * @return
	 */
	public List<Episode> getEpisodes()
	{
		// If we have not assigned episodes, 
		// at tempt to get episodes or initialize an empty array.
		if (episodes == null)
		{
			if (_embedded != null)
			{
				episodes = _embedded.getEpisodes();
				episodes.stream().forEach(e ->
				{
					e.setShowId(id);
				});
			}
			else
			{
				episodes = new ArrayList<>();
			}
		}

		return episodes;
	}

	public void setEpisodes(List<Episode> episodes)
	{
		// Update episodes with Show ID
		episodes.stream().forEach(e ->
		{
			e.setShowId(id);
		});

		Collections.sort(episodes); // Sort episodes
		this.episodes = episodes;
	}

	/**
	 * Get/Set previous episode for the showw
	 *
	 * @return
	 */
	public Episode getPreviousEpisode()
	{
		if (previousEpisode == null)
		{
			previousEpisode = new Episode();
		}
		return previousEpisode;
	}

	public void setPreviousEpisode(Episode previousEpisode)
	{
		if (previousEpisode != null)
		{
			previousEpisode.setShowId(id);
		}
		this.previousEpisode = previousEpisode;
	}

	/**
	 * Returns a Json String of this object
	 *
	 * @return
	 */
	public String toJson()
	{
		return GSON.toJson(this);
	}

	/**
	 * Comparable
	 *
	 * @param o
	 * @return
	 */
	@Override
	public int compareTo(Show o)
	{
		/**
		 * We do not want to sort by "the": Remove it before comparing
		 */
		return name.replaceAll("(?i)^the\\s", "").compareTo(o.name.replaceAll("(?i)^the\\s", ""));
	}

	/**
	 * toString
	 *
	 * @return
	 */
	@Override
	public String toString()
	{
		return name;
	}

}
