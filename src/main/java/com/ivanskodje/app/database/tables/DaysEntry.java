/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivanskodje.app.database.tables;

/**
 *
 * @author Ivan Skodje
 */
public class DaysEntry
{

	// Table name
	public static final String TABLE_NAME = "days";

	// Table id
	public static final String COLUMN_SHOW_ID = "show_id";

	// Table name
	public static final String COLUMN_DAY = "day";
}
