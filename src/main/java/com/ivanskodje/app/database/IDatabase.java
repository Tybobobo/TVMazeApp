/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivanskodje.app.database;

import com.ivanskodje.tvmazeapp.model.Episode;
import com.ivanskodje.tvmazeapp.model.Network;
import com.ivanskodje.tvmazeapp.model.Show;
import com.ivanskodje.tvmazeapp.model.ShowSchedule;
import java.sql.ResultSet;
import java.util.List;

/**
 *
 * @author Ivan Skodje
 */
public interface IDatabase
{

	/* Executes a query, returns the resultset */
	public ResultSet executeQuery(String query);

	/* Executes a query action, returns a boolean to determine success or failure */
	public boolean executeAction(String query);

	// POPULATION
	/* Populate Database */
	public boolean populateDatabase();

	// NETWORK
	/* Insert a network into database*/
	public Network insertNetwork(Network network);

	/* Updates a network that is already in the database */
	public boolean updateNetwork(Network network);

	/* Removes a network from database */
	public boolean deleteNetwork(int networkId);

	/* Returns network that matches id */
	public Network getNetwork(int networkId);

	/* Returns all network in a List */
	public List<Network> getAllNetworks();

	// SHOW
	/* Insert a Show into database*/
	public Show insertShow(Show show);

	/* Used to insert populated data from weekly episodes */
	public boolean insertWeeklyShowSchedules(List<ShowSchedule> showSchedules);

	/* Updates a Show that is already in the database */
	public boolean updateShow(Show show);

	/* Removes a List from database */
	public boolean deleteShow(int showId);

	/* Returns Show that matches id */
	public Show getShow(int showId);

	/* Returns all Shows in a List */
	public List<Show> getAllShows();

	/* Returns all Shows in a List */
	public List<ShowSchedule> getWeeklyShowSchedules();

	/* Returns all Shows that matches Network Id */
	public List<Show> getAllShowsWithNetworkId(int networkId);

	// EPISODE
	/* Insert an Episode into database, and returns the same Episode */
	public Episode insertEpisode(Episode episode);

	/* Updates a Episode that is already in the database */
	public boolean updateEpisode(Episode episode);

	/* Removes an Episode from database */
	public boolean deleteEpisode(int episodeId);

	/* Returns Episode that matches id */
	public Episode getEpisode(int episodeId);

	/* Returns all Episodes in a List */
	public List<Episode> getAllEpisodes();

	/* Returns all Shows that matches List Id */
	public List<Episode> getAllEpisodesWithShowId(int showId);
}
