/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivanskodje.tvmazeapp.model;

/**
 *
 * @author Ivan Skodje
 */
public class ShowSchedule implements Comparable<ShowSchedule>
{

	// The show
	Show show;

	// Days with S##E## information
	String empty = ""; // -, *, Empty, etc.
	Episode[] episodeDays = new Episode[]
	{
		null, null, null, null, null, null, null
	};

	/**
	 * Use constructor to get all Shows and store
	 *
	 * @param show
	 */
	public ShowSchedule(Show show)
	{
		this.show = show;
	}

	public int getShowId()
	{
		return show.getId();
	}

	public Show getShow()
	{
		if (show == null)
		{
			show = new Show();
		}
		return show;
	}

	public void addDay(int dayIndex, Episode episode)
	{
		getEpisodeDays()[dayIndex] = episode;
	}

	public Episode[] getEpisodeDays()
	{
		return episodeDays;
	}

	@Override
	public int compareTo(ShowSchedule o)
	{
		return getShow().compareTo(o.getShow());
	}

}
