/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivanskodje.tvmazeapp.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Embedded contains data you may find with &embedded URLs.
 *
 * @author Ivan Skodje
 */
public class Embedded
{

	private List<Episode> episodes;

	public List<Episode> getEpisodes()
	{
		if (episodes == null)
		{
			episodes = new ArrayList<>();
		}
		return episodes;
	}

}
