/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivanskodje.tvmazeapp.model;

/**
 *
 * @author Ivan Skodje
 */
public class Externals
{

	private int tvrage;
	private int thetvdb;
	private String imdb;

	public int getTvrage()
	{
		return tvrage;
	}

	public int getThetvdb()
	{
		return thetvdb;
	}

	public String getImdb()
	{
		if (imdb == null)
		{
			imdb = "";
		}
		return imdb;
	}

	@Override
	public String toString()
	{
		return "tvrage: " + tvrage + ", thetvdb: " + thetvdb + ", imdb: " + imdb;
	}

}
