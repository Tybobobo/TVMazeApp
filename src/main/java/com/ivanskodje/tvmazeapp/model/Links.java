/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivanskodje.tvmazeapp.model;

import java.util.Map;

/**
 *
 * @author Ivan Skodje
 */
public class Links
{

	// GSON De/Serialization properties
	private Map<String, String> self;
	private Map<String, String> previousepisode;

	// STATIC KEYS
	private static final String KEY_SELF = "href";
	private static final String KEY_PREVIOUS_EPISODE = "href";

	public String getPreviousEpisode()
	{
		if (previousepisode == null)
		{
			return "";
		}
		return previousepisode.get(KEY_PREVIOUS_EPISODE);
	}

	public String getSelf()
	{
		if (self == null)
		{
			return "";
		}
		return self.get(KEY_SELF);
	}
}
