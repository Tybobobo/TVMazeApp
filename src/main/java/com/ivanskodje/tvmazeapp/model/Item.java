/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivanskodje.tvmazeapp.model;

/**
 *
 * @author Ivan Skodje
 */
public class Item
{

	// Item Score
	private float score;

	// Item Show
	private Show show;

	/**
	 * The Show's rating is measured by the score
	 *
	 * @return
	 */
	public float getScore()
	{
		return score;
	}

	/**
	 * The show item
	 *
	 * @return
	 */
	public Show getShow()
	{
		if (show == null)
		{
			show = new Show();
		}
		return show;
	}
}
