/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivanskodje.app.database.tables;

/**
 *
 * @author Ivan Skodje
 */
public final class NetworkEntry
{

	// Table name
	public static final String TABLE_NAME = "network";

	// Table id
	public static final String COLUMN_ID = "id";

	// Table name
	public static final String COLUMN_NAME = "name";

	// Table country
	public static final String COLUMN_COUNTRY = "country";
}
