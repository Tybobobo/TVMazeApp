/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivanskodje.tvmazeapp;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.ivanskodje.tvmazeapp.model.Episode;
import com.ivanskodje.tvmazeapp.model.Item;
import com.ivanskodje.tvmazeapp.model.Show;
import com.ivanskodje.tvmazeapp.model.ShowSchedule;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.yamj.api.common.exception.ApiException;
import org.yamj.api.common.http.DigestedResponse;

/**
 * TVMaze API will act as a singleton and Fascade for accessing the TVMaze API
 * with as much ease as possible.
 *
 * @author Ivan Skodje
 */
public class TVMazeApi
{

	// Our instance
	private static TVMazeApi instance;

	// TVMazeHttp handles interaction with the TVMaze server
	private static TVMazeHttp tvMazeHttp;

	// Gson : Used for de/serializing json content
	private static final Gson GSON = new Gson();

	// TVMaze API URLs
	private static final String API_SEARCH_SHOWS = "http://api.tvmaze.com/search/shows?q=%s";
	private static final String API_SEARCH_SINGLE_SHOW = "http://api.tvmaze.com/singlesearch/shows?q=%s";
	private static final String API_SEARCH_SINGLE_SHOW_WITH_EPISODES = "http://api.tvmaze.com/singlesearch/shows?q=%s&embed=episodes";
	private static final String API_GET_EPISODES_FROM_SHOW = "http://api.tvmaze.com/shows/%s/episodes";
	private static final String API_GET_SHOW_BY_INDEX = "http://api.tvmaze.com/shows?page=%s";
	private static final String API_GET_EPISODES_BY_SHOW_AND_DATE = "http://api.tvmaze.com/shows/%s/episodesbydate?date=%s";
	private static final String API_GET_EPISODE_AND_SHOW_SCHEDULES_BY_DATE = "http://api.tvmaze.com/schedule?date=%s";

	/**
	 * Singleton getInstance()
	 *
	 * @return
	 */
	public static TVMazeApi getInstance()
	{
		// Lazy instantiation
		if (instance == null)
		{
			try
			{
				instance = new TVMazeApi();
			}
			catch (ApiException ex)
			{
				System.err.println("ApiException: " + ex.getMessage());
			}
		}
		return instance;
	}

	/**
	 * Private Constructor prevents instantiation from other classes. Two
	 * constructors for flexibility.
	 *
	 * @throws ApiException
	 */
	private TVMazeApi() throws ApiException
	{
		this(new TVMazeHttp());
	}

	private TVMazeApi(TVMazeHttp tvMazeHttp)
	{
		TVMazeApi.tvMazeHttp = tvMazeHttp;
	}

	/**
	 * Use a query to search and return a list of matching shows. If none are
	 * found, it returns an empty list.
	 *
	 * @param query
	 * @return
	 */
	public List<Show> searchShows(String query)
	{
		return searchShows(query, false);
	}

	/**
	 * Use a query to search and return a list of matching shows. If none are
	 * found, it returns an empty list.
	 *
	 * @param query
	 * @param getEpisodes
	 * @return
	 */
	public List<Show> searchShows(String query, boolean getEpisodes)
	{
		try
		{
			// Get Json content that contains the show information
			String jsonContent = getJsonContent(Arrays.asList(query), API_SEARCH_SHOWS);

			// Get content type as List<Item>
			Type collectionType = new TypeToken<Collection<Item>>()
			{
			}.getType();
			List<Item> lists = GSON.fromJson(jsonContent, collectionType);

			// For each show in the List, we append on a new List
			List<Show> shows = new ArrayList<>();
			lists.stream().forEach(item ->
			{
				// Get show from Item
				Show show = item.getShow();

				// Add all the Episodes that belong to Show
				if (getEpisodes)
				{
					show.setEpisodes(getEpisodes(show));
				}

				// Add show to List<Show>
				shows.add(item.getShow());
			});

			// Return List<Show>
			return shows;
		}
		catch (JsonSyntaxException ex)
		{
			System.err.println("JsonSyntaxException: " + ex.getMessage());
		}

		// If all fails, return empty ArrayList
		return new ArrayList<>();
	}

	/**
	 * Use a query to search and return one matching show. If none are found
	 * return null.
	 *
	 * @param query
	 * @return
	 */
	public Show searchSingleShow(String query)
	{
		return searchSingleShow(query, false);
	}

	/**
	 * Use a query to search and return one matching show. If none are found
	 * return null.
	 *
	 * @param query
	 * @param getEpisodes
	 * @return
	 */
	public Show searchSingleShow(String query, boolean getEpisodes)
	{
		try
		{
			// Whether or not we want to get episodes with call
			String apiUrl = getEpisodes ? API_SEARCH_SINGLE_SHOW_WITH_EPISODES : API_SEARCH_SINGLE_SHOW;

			// Get Json content that contains the show information
			String jsonContent = getJsonContent(Arrays.asList(query), apiUrl);

			// Get content type as List<Item>
			Show show = GSON.fromJson(jsonContent, Show.class);

			// Return List<Show>
			return show;
		}
		catch (JsonSyntaxException ex)
		{
			System.err.println("JsonSyntaxException: " + ex.getMessage());
		}

		return null;
	}

	/**
	 * Returns a List<Show> of 250 or less.
	 *
	 * @param index
	 * @param getEpisodes
	 * @return
	 */
	public List<Show> getShowByIndex(int index, boolean getEpisodes)
	{
		try
		{
			// Get Json content that contains the show information
			String jsonContent = getJsonContent(Arrays.asList(index + ""), API_GET_SHOW_BY_INDEX);

			// Get content type as List<Item>
			Type collectionType = new TypeToken<Collection<Show>>()
			{
			}.getType();
			List<Show> shows = GSON.fromJson(jsonContent, collectionType);

			// Get all episodes with the shows
			if (getEpisodes)
			{
				shows.stream().forEach(show ->
				{
					show.setEpisodes(getEpisodes(show));
				});
			}

			// Return List<Show>
			return shows;
		}
		catch (JsonSyntaxException ex)
		{
			System.err.println("JsonSyntaxException: " + ex.getMessage());
		}

		// If all fails, return empty ArrayList
		return new ArrayList<>();
	}

	/**
	 * Returns all the episodes that belongings to the show.
	 *
	 * @param show
	 * @return
	 */
	public List<Episode> getEpisodes(Show show)
	{
		return getEpisodes(show.getId());
	}

	/**
	 * Returns all the episodes that belongings to the show.
	 *
	 * @param showId
	 * @return
	 */
	public List<Episode> getEpisodes(int showId)
	{
		// Get Json content
		String jsonContent = getJsonContent(Arrays.asList("" + showId), API_GET_EPISODES_FROM_SHOW);

		// Get content type as List<Episode>
		Type collectionType = new TypeToken<Collection<Episode>>()
		{
		}.getType();
		List<Episode> episodes = GSON.fromJson(jsonContent, collectionType);

		// Return List<Episode>
		return episodes;
	}

	/**
	 * Returns the Episodes on a given Show and Date
	 *
	 * @param show
	 * @param date
	 * @return
	 */
	public List<Episode> getEpisodesByShowAndDate(Show show, Date date)
	{
		// Get Json content
		String jsonContent = getJsonContent(Arrays.asList(show.getId() + "", date.toString()), API_GET_EPISODES_BY_SHOW_AND_DATE);

		// Get content type as List<Episode>
		Type collectionType = new TypeToken<Collection<Episode>>()
		{
		}.getType();
		List<Episode> episodes = GSON.fromJson(jsonContent, collectionType);

		// Return List<Episode>
		return episodes;
	}

	/**
	 * Returns a List of weekly episodes
	 *
	 * @return
	 */
	public List<ShowSchedule> getWeeklyShowSchedules()
	{
		// ---- Getting Weekly Episodes ----
		// Load this weeks Shows from TVMaze API
		// Get currentWeek Start and End
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setFirstDayOfWeek(Calendar.MONDAY); // Start with Monday

		// Set the initial day to be Monday (this week)
		calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

		// Lists of List<Episode>
		List<List<Episode>> weeklyEpisodes = new ArrayList<>();
		for (int i = 0; i < 7; i++)
		{
			// Get a List of all Episodes (and Shows) for each day of the week
			weeklyEpisodes.add(new ArrayList<>(getEpisodesAndShowSchedulesByDate(calendar.getTime())));

			// Next day
			calendar.add(Calendar.DATE, 1);
		}

		// ---- Setting up List<ShowSchedule> ----
		// Show ID, ShowSchedule Map
		Map<Integer, ShowSchedule> showSchedules = new HashMap<>();

		// Get shows that run this week
		for (int day = 0; day < 7; day++)
		{
			// Get the episodes for the day
			List<Episode> dailyEpisodes = weeklyEpisodes.get(day); // 0 = Monday, 1 = Tuesday, (...)

			// Episodes for Day [#]
			for (Episode episode : dailyEpisodes)
			{
				Show show = episode.getShow();
				int showId = show.getId();

				// Store the Show in our ShowSchedule HashMap (if not already added)
				if (!showSchedules.containsKey(showId))
				{
					// Add new Show with schedule
					showSchedules.put(showId, new ShowSchedule(show));
				}

				try
				{
					// Add schedule
					showSchedules.get(showId).addDay(day, episode);
				}
				catch (NullPointerException ex)
				{
					// Not defined in Json, add empty episode
					showSchedules.get(showId).addDay(day, new Episode());
				}
			}
		}

		// Sorted listing
		List<ShowSchedule> sortedSchedule = new ArrayList<>(showSchedules.values());
		Collections.sort(sortedSchedule);

		// Return List<ShowSchedule>
		return sortedSchedule;
	}

	/**
	 * Used for NextWeek report:
	 *
	 * We use this to retrieve all shows and episodes on a certain date. We will
	 * get all episodes, with a reference to the Show it contains in.
	 *
	 * @param date
	 * @return
	 */
	public List<Episode> getEpisodesAndShowSchedulesByDate(Date date)
	{
		// Format string
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String formattedDate = dateFormat.format(date);

		// Get Json content
		String jsonContent = getJsonContent(Arrays.asList(formattedDate + ""), API_GET_EPISODE_AND_SHOW_SCHEDULES_BY_DATE);

		// Get content type as List<Episode>
		Type collectionType = new TypeToken<Collection<Episode>>()
		{
		}.getType();
		List<Episode> episodes = GSON.fromJson(jsonContent, collectionType);

		// Return List<Episode>
		return episodes;
	}

	/**
	 * Used to retrieve the Json content from the TVMazeHttp DigestedResponse
	 * with getPage([query], urlText).
	 *
	 * @param query
	 * @param urlText
	 * @return
	 */
	private String getJsonContent(List<String> queries, String urlText)
	{
		// Request page response
		DigestedResponse response = tvMazeHttp.getPage(queries, urlText);

		// Get Json content that contains the show information
		String jsonContent = response.getContent();

		// Return content
		return jsonContent;
	}

}
