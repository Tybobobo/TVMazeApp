/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivanskodje.app.database;

import com.ivanskodje.app.helpers.FileManagers.JsonFileManager;
import java.time.LocalDate;

/**
 * Note: This is where we would store the time of last time we synced with the
 * server.
 *
 * @author Ivan Skodje
 */
public class DatabaseConfig
{

	// Path to file
	private static final transient String DB_CONFIG_PATH = "db_config.json";

	// Instance (transient = Gson ignores this)
	private static transient DatabaseConfig instance;

	// Index
	private Integer index;

	private LocalDate dayLoaded;

	/**
	 * Get instance from singleton
	 *
	 * @return
	 */
	public static DatabaseConfig getInstance()
	{
		if (instance == null)
		{
			instance = JsonFileManager.loadFile(DB_CONFIG_PATH, DatabaseConfig.class);
		}
		return instance;
	}

	/**
	 * Index is used to keep track of how many indexes we have loaded to
	 * database. This prevents us from reloading the same content over and over,
	 * when used correctly.
	 *
	 * @return
	 */
	public int getIndex()
	{
		if (index == null)
		{
			index = -1;
		}
		return index;
	}

	public void setIndex(int index)
	{
		this.index = index;
	}

	public LocalDate getDayLoaded()
	{
		return dayLoaded;
	}

	public void setDayLoaded(LocalDate dayLoaded)
	{
		this.dayLoaded = dayLoaded;
	}

	/**
	 * Increments index by 1
	 */
	public void incrementIndex()
	{
		index++;
	}

	/**
	 * Save current config state
	 *
	 * @return
	 */
	public boolean save()
	{
		return JsonFileManager.saveFile(DB_CONFIG_PATH, DatabaseConfig.class);
	}
}
