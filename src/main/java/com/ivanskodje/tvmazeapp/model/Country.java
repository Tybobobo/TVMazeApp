/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivanskodje.tvmazeapp.model;

/**
 *
 * @author Ivan Skodje
 */
public class Country implements Comparable<Country>
{

	private String name;
	private String code;
	private String timezone; // Ex: "America/Net_Work"

	public String getName()
	{
		if (name == null)
		{
			name = "";
		}
		return name;
	}

	public String getCode()
	{
		if (code == null)
		{
			code = "";
		}
		return code;
	}

	public String getTimezone()
	{
		if (timezone == null)
		{
			timezone = "";
		}
		return timezone;
	}

	public void setTimezone(String timezone)
	{
		this.timezone = timezone;
	}

	@Override
	public int compareTo(Country o)
	{
		return getName().compareTo(o.getName());
	}

	@Override
	public String toString()
	{
		return getName();
	}
}
