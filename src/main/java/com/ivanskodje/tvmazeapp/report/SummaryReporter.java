/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivanskodje.tvmazeapp.report;

import com.ivanskodje.tvmazeapp.model.Show;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Ivan Skodje
 */
public class SummaryReporter implements Reporter
{

	private static final String[] COLUMN_HEADERS = new String[]
	{
		"SHOW_NAME",
		"NETWORK",
		"GENRES",
		"EPISODE_COUNT"

	};

	/**
	 * Takes in Shows
	 *
	 * @param items
	 * @return
	 */
	@Override
	public String format(List items)
	{
		List<Show> shows = (List<Show>) items;

		if (shows == null)
		{
			return "";
		}
		// Prepare string builder
		StringBuilder stringBuilder = new StringBuilder();
		String columnFormat = "%s;%s;%s;%s\n";

		// Add headers
		String headers = String.format(columnFormat, (Object[]) COLUMN_HEADERS);
		stringBuilder.append(headers);

		// Sorted listing
		Collections.sort(shows);

		// Iterate through finalized schedules, and append to string builder
		for (Show show : shows)
		{
			// Put together row
			String row = String.format(columnFormat, show.getName(), show.getNetwork(), show.getGenres(), show.getEpisodes().size());
			stringBuilder.append(row);
		}

		// Return NextWeek formatted text
		return stringBuilder.toString();
	}
}
