/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivanskodje.tvmazeapp.model;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Ivan Skodje
 */
public class Episode implements Comparable<Episode>
{

	// Gson
	private static final Gson GSON = new Gson();

	// Properties
	private int id;
	private String url;
	private String name;
	private Integer season; // Integer is used in order to know when it has been set (not null)
	private Integer number;
	private String airdate;
	private String airtime;
	private int runtime;
	private Map<String, String> image;
	private String summary;
	private Links _links;

	@Expose(serialize = false, deserialize = true)
	private Show show; // Only retrieved when we deserialize as we dont want to store this in Json

	// Manipulated values
	private int showId;
	private LocalDate airDate;
	private LocalTime airTime;

	public int getId()
	{
		return id;
	}

	public String getUrl()
	{
		if (url == null)
		{
			url = "";
		}
		return url;
	}

	public String getName()
	{
		if (name == null)
		{
			name = "";
		}
		return name;
	}

	public Integer getSeason()
	{
		if (season == null)
		{
			return -1; // We do not set season, as we use it for comparison
		}
		return season;
	}

	public Integer getNumber()
	{
		if (number == null)
		{
			return -1; // We do not set season, as we use it for comparison
		}
		return number;
	}

	public LocalDate getAirDate()
	{
		try
		{
			if (airdate != null && airDate == null)
			{
				airDate = LocalDate.parse(airdate);
			}
			return airDate;
		}
		catch (DateTimeParseException ex)
		{
			System.err.println("DateTimeParseException (" + ex.getClass().getName() + "):\n" + ex.getMessage());
		}

		return null;
	}

	public LocalTime getAirTime()
	{
		try
		{
			if (airtime != null && airTime == null)
			{
				airTime = LocalTime.parse(airtime);
			}
			return airTime;
		}
		catch (DateTimeParseException ex)
		{
			System.err.println("DateTimeParseException (" + ex.getClass().getName() + "):\n" + ex.getMessage());
		}

		return null;

	}

	public int getRuntime()
	{
		return runtime;
	}

	public Map<String, String> getImage()
	{
		if (image == null)
		{
			image = new HashMap<>();
		}
		return image;
	}

	public String getSummary()
	{
		if (summary == null)
		{
			summary = "";
		}
		return summary;
	}

	public Links getLinks()
	{
		return _links;
	}

	/**
	 * Get/Set the show ID this episode belong in
	 *
	 * @return
	 */
	public int getShowId()
	{
		return showId;
	}

	public void setShowId(int showId)
	{
		this.showId = showId;
	}

	/**
	 * This is available only when we pull Schedules from TVMaze API.
	 *
	 * @return
	 */
	public Show getShow()
	{
		if (show == null)
		{
			show = new Show();
		}
		return show;
	}

	/**
	 * Returns a Json String of this object
	 *
	 * @return
	 */
	public String toJson()
	{
		return GSON.toJson(this);
	}

	/**
	 * Sorting episodes
	 *
	 * @param o
	 * @return
	 */
	@Override
	public int compareTo(Episode o)
	{
		/**
		 * We check to see if we are in the same show. If we are comparing
		 * episodes that are NOT in the show, we compare them by date aired.
		 */
		if (showId == o.showId)
		{
			if (season != null && number != null && o.season != null && o.number != null)
			{
				/**
				 * In order to get a list of all episodes from each season, we
				 * will need to check if we are in same season and compare
				 * numbers, else we sort them by seasons.
				 *
				 * Ex: S1E1, S1E2, S2E1, S2E5, S3E2 ...
				 */
				if (season == o.season)
				{
					return number.compareTo(o.number);
				}
				else
				{
					return season.compareTo(o.season);
				}
			}
		}
		else
		{
			if (airdate != null && o.airdate != null)
			{
				return airdate.compareTo(o.airdate);
			}
		}

		/**
		 * If all fails, compare them by their ID
		 */
		return Integer.compare(id, o.id);
	}

	@Override
	public String toString()
	{
		return "S" + String.format("%02d", getSeason()) + "E" + String.format("%02d", getNumber());
	}

}
