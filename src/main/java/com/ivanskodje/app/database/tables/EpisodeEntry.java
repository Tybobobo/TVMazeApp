/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivanskodje.app.database.tables;

/**
 *
 * @author Ivan Skodje
 */
public final class EpisodeEntry
{

	// Table name
	public static final String TABLE_NAME = "episode";

	// Table id
	public static final String COLUMN_ID = "id";

	// Table show id : The show this episode belongs in
	public static final String COLUMN_SHOW_ID = "show_id";

	// Table name
	public static final String COLUMN_NAME = "name";

	// Table season
	public static final String COLUMN_SEASON = "season";

	// Table number
	public static final String COLUMN_NUMBER = "number";

	// Table date
	public static final String COLUMN_AIRDATE = "airdate";

	// Table time
	public static final String COLUMN_AIRTIME = "airtime";

	// Table json content : Used to retrieve data we may not always want to load.
	public static final String COLUMN_JSON_CONTENT = "json_content";

	// Table date created
	public static final String COLUMN_DATE_CREATED = "date_created";
}
