/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivanskodje.app.helpers.FileManagers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;

/**
 * A simple file manager that will allow us to easily store and fetch data using
 * basic generics.
 *
 * @author Ivan Skodje
 */
public class JsonFileManager
{

	// Pretty Gson
	private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();

	/**
	 * Store a generic object to a file
	 *
	 * @param <T>
	 * @param filePath
	 * @param data
	 * @return
	 */
	public static <T> boolean saveFile(String filePath, T data)
	{
		FileWriter file = null;
		try
		{
			file = new FileWriter(filePath);
			file.write(GSON.toJson(data));
			return true;
		}
		catch (JsonSyntaxException | IOException ex)
		{
			Logger.getLogger(JsonFileManager.class.getName()).log(Level.SEVERE, null, ex);
		}
		finally
		{
			if (file != null)
			{
				try
				{
					file.close();
				}
				catch (IOException ex)
				{
					Logger.getLogger(JsonFileManager.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		}
		return false;
	}

	/**
	 * Load generic object from a file. Must be given expected Class for it to
	 * work.
	 *
	 * @param <T>
	 * @param filePath
	 * @param clazz
	 * @return
	 */
	public static <T> T loadFile(String filePath, Class<T> clazz)
	{

		T data = null;
		try
		{
			File file = new File(filePath);
			if (file.exists())
			{
				data = GSON.fromJson(FileUtils.readFileToString(file, "UTF-8"), clazz);
				return data;
			}
		}
		catch (JsonSyntaxException | IOException ex)
		{
			Logger.getLogger(JsonFileManager.class.getName()).log(Level.SEVERE, null, ex);
		}
		return data;
	}
}
