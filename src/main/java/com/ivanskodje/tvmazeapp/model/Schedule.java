/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivanskodje.tvmazeapp.model;

import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ivan Skodje
 */
public class Schedule
{

	private String time;
	private List<String> days;

	private LocalTime localTime;

	/**
	 * Get LocalTime
	 *
	 * @return
	 */
	public LocalTime getTime()
	{
		try
		{
			if (localTime == null)
			{
				localTime = LocalTime.parse(time);
			}
			return localTime;
		}
		catch (DateTimeParseException ex)
		{
			System.err.println("DateTimeParseException (" + ex.getClass().getName() + "):\n" + ex.getMessage());
		}

		return null;
	}

	public List<String> getDays()
	{
		if (days == null)
		{
			days = new ArrayList<>();
		}
		return days;
	}

	@Override
	public String toString()
	{
		// Get days
		List<String> schbeduleDays = getDays();

		// Prepare empty string
		String toString = "";

		// Append Days
		for (int i = 0; i < schbeduleDays.size(); i++)
		{
			if (i != schbeduleDays.size() - 1)
			{
				toString += schbeduleDays.get(i) + ", ";
			}
			else
			{
				toString += schbeduleDays.get(i) + " at ";
			}
		}

		// Append LocalTime
		toString += getTime();

		// Return toString
		return toString;
	}

}
