/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivanskodje.app.database;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ivanskodje.tvmazeapp.model.Episode;
import com.ivanskodje.tvmazeapp.model.Network;
import com.ivanskodje.tvmazeapp.model.Show;
import com.ivanskodje.tvmazeapp.model.ShowSchedule;
import java.util.List;

/**
 * DataHandlers behave like a singleton and a fascade to simplify interaction
 * with the database, regardless of which Database you use.
 *
 *
 * @author Ivan Skodje
 */
public class DataHandler
{

	// Our instance
	private static DataHandler instance;

	// Database
	private static IDatabase database = null;

	// Gson 
	private final static Gson GSON = new GsonBuilder().setPrettyPrinting().create();

	// Database Name
	private final static String DATABASE_NAME = "LocalDatabase";

	/**
	 * Singleton Instance
	 *
	 * @return
	 */
	public static DataHandler getInstance()
	{
		if (instance == null)
		{
			instance = new DataHandler(new SQLiteDatabase(DATABASE_NAME));
		}
		return instance;
	}

	/**
	 * Load all Shows, Networks and Episodes from the TVMaze Network and
	 * populate the database for use locally.
	 *
	 * @return
	 */
	public boolean populateDatabase()
	{
		return database.populateDatabase();
	}

	/**
	 * Private constructor
	 *
	 * @param database
	 */
	private DataHandler(IDatabase database)
	{
		DataHandler.database = database;
	}

	/* Insert a network into database */
	public Network insertNetwork(Network network)
	{
		return database.insertNetwork(network);
	}

	/* Updates a network that is already in the database */
	public boolean updateNetwork(Network network)
	{
		return database.updateNetwork(network);
	}

	/* Removes a network from database */
	public boolean deleteNetwork(int networkId)
	{
		return database.deleteNetwork(networkId);
	}

	/* Returns network that matches id */
	public Network getNetwork(int networkId)
	{
		return database.getNetwork(networkId);
	}

	/* Returns all network in a List */
	public List<Network> getAllNetworks()
	{
		return database.getAllNetworks();
	}

	/* Insert Show */
	public Show insertShow(Show show)
	{
		return database.insertShow(show);
	}

	/* Insert a list of weekly show schedules */
	public boolean insertWeeklyShowSchedules(List<ShowSchedule> showSchedules)
	{
		return database.insertWeeklyShowSchedules(showSchedules);
	}

	/* Update show */
	public boolean updateShow(Show show)
	{
		return database.updateShow(show);
	}

	/* Delete show */
	public boolean deleteShow(int showId)
	{
		return database.deleteShow(showId);
	}

	/* Get show */
	public Show getShow(int showId)
	{
		return database.getShow(showId);
	}

	/* Get a list of all shows */
	public List<Show> getAllShows()
	{
		return database.getAllShows();
	}

	/* Get this week's show schedules */
	public List<ShowSchedule> getWeeklyShowSchedules()
	{
		return database.getWeeklyShowSchedules();
	}

	/* Insert episode */
	public Episode insertEpisode(Episode episode)
	{
		return database.insertEpisode(episode);
	}

	/* Update episode */
	public boolean updateEpisode(Episode episode)
	{
		return database.updateEpisode(episode);
	}

	/* Delete episode */
	public boolean deleteEpisode(int episodeId)
	{
		return database.deleteEpisode(episodeId);
	}

	/* Get episode */
	public Episode getEpisode(int episodeId)
	{
		return database.getEpisode(episodeId);
	}

	/* Get all episodes */
	public List<Episode> getAllEpisodes()
	{
		return database.getAllEpisodes();
	}

	/* Get all episodes that matches show ID */
	public List<Episode> getAllEpisodesWithShowId(int showId)
	{
		return database.getAllEpisodesWithShowId(showId);
	}
}
