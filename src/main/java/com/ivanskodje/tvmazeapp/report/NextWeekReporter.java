/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivanskodje.tvmazeapp.report;

import com.ivanskodje.tvmazeapp.model.Episode;
import com.ivanskodje.tvmazeapp.model.ShowSchedule;
import java.util.Collections;
import java.util.List;

/**
 * NextWeekReporter will format a list of Shows into a a Report, showing which
 * shows and episodes are running this week.
 *
 * @author Ivan Skodje
 */
public class NextWeekReporter implements Reporter
{

	private static final String[] COLUMN_HEADERS = new String[]
	{
		"SHOW_NAME",
		"MONDAY",
		"TUESDAY",
		"WEDNESDAY",
		"THURSDAY",
		"FRIDAY",
		"SATURDAY",
		"SUNDAY"

	};

	@Override
	public String format(List showSchedules)
	{
		// Prepare string builder
		StringBuilder stringBuilder = new StringBuilder();
		String columnFormat = "%s;%s;%s;%s;%s;%s;%s;%s\n";

		// Add headers
		String headers = String.format(columnFormat, (Object[]) COLUMN_HEADERS);
		stringBuilder.append(headers);

		// Sorted listing
		Collections.sort(showSchedules);

		// Iterate through finalized schedules, and append to string builder
		for (Object obj : showSchedules)
		{
			ShowSchedule schedule = (ShowSchedule) obj;
			Episode[] days = schedule.getEpisodeDays();

			// Put together row
			String row = String.format(columnFormat, schedule.getShow().getName(), days[0], days[1], days[2], days[3], days[4], days[5], days[6]);
			stringBuilder.append(row);
		}

		// Return NextWeek formatted text
		return stringBuilder.toString();
	}
}
