/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivanskodje.tvmazeapp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.yamj.api.common.http.DigestedResponse;

/**
 *
 * @author Ivan Skodje
 */
@Ignore
public class TVMazeHttpTest
{

	public TVMazeHttpTest()
	{
	}

	@BeforeClass
	public static void setUpClass()
	{
	}

	@AfterClass
	public static void tearDownClass()
	{
	}

	@Before
	public void setUp()
	{
	}

	@After
	public void tearDown()
	{
	}

	/**
	 * Test Single Search that retrieves one result.
	 *
	 * NB: Since we are not using Mock data, we need to be able to connect to
	 * TVMaze's web-site in order to run a successful test. (Mock data could
	 * also be provided using a local file.)
	 *
	 * @throws java.lang.Exception
	 */
	@Test
	public void testSingleSearchSuccess() throws Exception
	{
		List<String> queries = new ArrayList<>();
		queries.add("silicon valley");
		String apiUrl = "http://api.tvmaze.com/singlesearch/shows?q=%s";
		TVMazeHttp instance = new TVMazeHttp();

		DigestedResponse result = instance.getPage(queries, apiUrl);
		Assert.assertNotNull(result);
	}

	/**
	 * Test Single Search that does not provide any matching results.
	 *
	 * NB: Since we are not using Mock data, we need to be able to connect to
	 * TVMaze's web-site in order to run a successful test. (Mock data could
	 * also be provided using a local file.)
	 *
	 * @throws java.lang.Exception
	 */
	@Test
	public void testSingleSearchNoMatch() throws Exception
	{
		List<String> queries = new ArrayList<>();
		queries.add("gkjasdkjsa");
		String apiUrl = "http://api.tvmaze.com/singlesearch/shows?q=%s";
		TVMazeHttp instance = new TVMazeHttp();

		DigestedResponse result = instance.getPage(queries, apiUrl);
		Assert.assertNull(result);
	}

	/**
	 * Multiple Tests in order to test HTTP Status 429 handling.
	 *
	 *
	 * NB: Since we are not using Mock data, we need to be able to connect to
	 * TVMaze's web-site in order to run a successful test. (Mock data could
	 * also be provided using a local file.)
	 *
	 * @throws java.lang.Exception
	 */
	@Test
	public void testManualHttpStatus429() throws Exception
	{
		// Number of requests
		int numRequests = 3;

		// Max allowed wait time before cancelling
		int maxTimeBeforeCancel = 32;

		// Index URL
		String apiUrl = "http://api.tvmaze.com/shows?page=%s";
		TVMazeHttp instance = new TVMazeHttp();

		// Request a random Index
		Random random = new Random();

		// We attempt to fetch the same query over and over until we get an 429
		for (int i = 0; i < numRequests; i++)
		{
			// Get a random index
			int index = random.nextInt(100);

			// > [Request Number]: Requesting index [Random Index]
			System.out.println("> " + i + ": Requesting index " + index);

			/**
			 * For each iteration, we add an additional fail number, which will
			 * cause us to success on first run, one 429 on second run, two 429
			 * on third.
			 *
			 * ---- EXPECTED OUTPUT ----
			 *
			 * STATUS CODE: 200
			 *
			 * > 1: Requesting index [int]
			 *
			 * --- Waiting 1 seconds... !!
			 *
			 * STATUS CODE: (Manual) [int]
			 *
			 * --- Waiting 2 seconds... !!
			 *
			 * STATUS CODE: 200
			 *
			 * > 2: Requesting index [int]
			 *
			 * --- Waiting 1 seconds... !!
			 *
			 * STATUS CODE: (Manual) [int]
			 *
			 * --- Waiting 2 seconds... !!
			 *
			 * STATUS CODE: (Manual) [int]
			 *
			 * --- Waiting 4 seconds... !!
			 *
			 * STATUS CODE: 200
			 */
			instance.testGetPage429Status(Arrays.asList(index + ""), apiUrl, i, maxTimeBeforeCancel);
		}
	}

	/**
	 * Multiple Tests in order to test HTTP Status 429 handling.
	 *
	 *
	 * NB: Since we are not using Mock data, we need to be able to connect to
	 * TVMaze's web-site in order to run a successful test. (Mock data could
	 * also be provided using a local file.)
	 *
	 * @throws java.lang.Exception
	 */
	@Test
	public void testManualCancelCallOnHttpStatus429() throws Exception
	{
		// Number of requests
		int numRequests = 3;

		// Max allowed wait time before cancelling
		int maxTimeBeforeCancel = 4;

		// Index URL
		String apiUrl = "http://api.tvmaze.com/shows?page=%s";
		TVMazeHttp instance = new TVMazeHttp();

		/**
		 * ---- EXPECTED OUTPUT ----
		 *
		 * > 1: Requesting index 0
		 *
		 * --- Waiting 1 seconds...
		 *
		 * STATUS CODE: (Manual) 429
		 *
		 * --- Waiting 2 seconds...
		 *
		 * STATUS CODE: (Manual) 429
		 *
		 * --- Waiting 4 seconds...
		 *
		 * Exception in Test Method:
		 *
		 * We are unable to get the TVMaze page you requested. Please try again
		 * at a later time.
		 */
		System.out.println("> 1: Requesting index " + 0);
		instance.testGetPage429Status(Arrays.asList(0 + ""), apiUrl, numRequests, maxTimeBeforeCancel); // Fail 3 times, but cancel when wait time exceed 4 seconds (more than 3 attempts)
	}

	/**
	 * Load all indexes and get out of range, which will give us a HTTP Status
	 * 404.
	 *
	 * NB: Since we are not using Mock data, we need to be able to connect to
	 * TVMaze's web-site in order to run a successful test. (Mock data could
	 * also be provided using a local file.)
	 *
	 */
	@Test
	public void testHttpStatus404()
	{
		try
		{
			// Requests over index 129 will cause 404 (At date 16.09.17)
			int numRequests = 10;

			// Index URL
			String apiUrl = "http://api.tvmaze.com/shows?page=%s";
			TVMazeHttp instance = new TVMazeHttp();

			// Interate from 0 and up to numRequests, and exceed number of indexes
			for (int i = 0; i < numRequests; i++)
			{
				System.out.println("> Request Number: " + (i + 1) + ", calling index: " + (i + 130));
				instance.getPage(Arrays.asList((i + 130) + ""), apiUrl);
			}
		}
		catch (Exception ex)
		{
			Assert.fail("Was unable to handle the error caused by requesting all indexes in TVMaze API:\n" + ex.getMessage());
		}
	}

	/**
	 * Check if MalformedURLException is handled. We have succeeded when result
	 * is null on this test.
	 *
	 * @throws Exception
	 */
	@Test
	public void testMalformedURLException() throws Exception
	{
		List<String> queries = new ArrayList<>();
		queries.add("test");
		String apiUrl = "a-f_#:";
		TVMazeHttp instance = new TVMazeHttp();

		// Because we use an incorrect URL, it will handle the 
		// MalformedURLException and simply return null.
		DigestedResponse response = instance.getPage(queries, apiUrl);

		// Make sure we got null
		Assert.assertNull(response);
	}

	/**
	 * Check if IOException is handled. We have succeeded when result is null on
	 * this test.
	 *
	 * @throws Exception
	 */
	@Test
	public void testIOException() throws Exception
	{
		List<String> queries = new ArrayList<>();
		queries.add("test");
		String apiUrl = "http://localtest/";
		TVMazeHttp instance = new TVMazeHttp();

		// Attempt to get Page
		DigestedResponse response = instance.getPage(queries, apiUrl);

		// Make sure we got null
		Assert.assertNull(response);
	}

	/**
	 * Check if URISyntaxException is handled. We have succeeded when result is
	 * null on this test.
	 *
	 * @throws Exception
	 */
	@Test
	public void testURISyntaxException() throws Exception
	{
		List<String> queries = new ArrayList<>();
		queries.add("test");
		String apiUrl = "http://localtest/h?s=^IXIC";
		TVMazeHttp instance = new TVMazeHttp();

		// Attempt to get Page
		DigestedResponse response = instance.getPage(queries, apiUrl);

		// Make sure we got null
		Assert.assertNull(response);
	}

	/**
	 * Check if MalformedURLException is handled using EMPTY strings. We have
	 * succeeded when result is null on this test.
	 *
	 * @throws Exception
	 */
	@Test
	public void testEmptyStrings() throws Exception
	{
		DigestedResponse response;
		List<String> queries = new ArrayList<>();
		queries.add("");
		String apiUrl = "";
		TVMazeHttp instance = new TVMazeHttp();

		// Attempt to get Page
		response = instance.getPage(queries, apiUrl);

		// Make sure we got null
		Assert.assertNull(response);
	}

	/**
	 * Check if URISyntaxException is handled using empty strings. We have
	 * succeeded when result is null on this test.
	 *
	 * @throws Exception
	 */
	@Test
	public void testNullVariables() throws Exception
	{
		DigestedResponse response;
		List<String> queries = null;
		String apiUrl = null;
		TVMazeHttp instance = new TVMazeHttp();

		// Attempt to get Page
		response = instance.getPage(queries, apiUrl);

		// Make sure we got null
		Assert.assertNull(response);
	}
}
