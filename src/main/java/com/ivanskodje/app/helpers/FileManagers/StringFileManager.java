/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivanskodje.app.helpers.FileManagers;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author Ivan Skodje
 */
public class StringFileManager
{

	/**
	 * Store a generic object to a file
	 *
	 * @param <T>
	 * @param filePath
	 * @param content
	 * @return
	 */
	public static <T> boolean saveFile(String filePath, String content)
	{
		FileWriter file = null;
		try
		{
			file = new FileWriter(filePath);
			file.write(content);
			return true;
		}
		catch (IOException ex)
		{
			Logger.getLogger(StringFileManager.class.getName()).log(Level.SEVERE, null, ex);
		}
		finally
		{
			if (file != null)
			{
				try
				{
					file.close();
				}
				catch (IOException ex)
				{
					Logger.getLogger(StringFileManager.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		}
		return false;
	}

	/**
	 * Load generic object from a file. Must be given expected Class for it to
	 * work.
	 *
	 * @param filePath
	 * @return
	 */
	public static String loadFile(String filePath)
	{
		try
		{
			File file = new File(filePath);
			if (file.exists())
			{
				return FileUtils.readFileToString(file, "UTF-8");
			}
		}
		catch (IOException ex)
		{
			Logger.getLogger(StringFileManager.class.getName()).log(Level.SEVERE, null, ex);
		}
		return "";
	}
}
